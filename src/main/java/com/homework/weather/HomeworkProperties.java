package com.homework.weather;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("homework")
public class HomeworkProperties {

	@Valid
	private final WeatherMap weatherMap = new WeatherMap();

	private List<String> locations = new ArrayList<>();

	public WeatherMap getWeatherMap() {
		return this.weatherMap;
	}
	
	public List<String> getLocations() {
		return this.locations;
	}

	public void setLocations(List<String> locations) {
		this.locations = locations;
	}

	public static class WeatherMap {

		@NotNull
		private String key;

		public String getKey() {
			return this.key;
		}

		public void setKey(String key) {
			this.key = key;
		}

	}

	
}

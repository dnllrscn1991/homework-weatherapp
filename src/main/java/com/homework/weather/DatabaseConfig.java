package com.homework.weather;

import javax.sql.DataSource;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;

@Configuration
public class DatabaseConfig {

	@Bean
	@ConfigurationProperties("spring.datasource")
	public DataSource dataSource() {
		return DataSourceBuilder.create().type(SimpleDriverDataSource.class).build();
	}

	@Bean
	public JdbcTemplate jdbcTemplate(DataSource datasource) {
		JdbcTemplate template = new JdbcTemplate();
		template.setDataSource(datasource);
		return template;
	}
}

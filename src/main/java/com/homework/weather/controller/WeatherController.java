package com.homework.weather.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.homework.weather.domain.WeatherInfo;
import com.homework.weather.service.WeatherService;

@RestController
@RequestMapping("/homework/weather")
public class WeatherController {
	
	private final WeatherService weatherService;
	
	
	public WeatherController(WeatherService weatherService) {
		this.weatherService = weatherService;
	}
	
	@RequestMapping("/now/{country}/{city}")
	public WeatherInfo getWeather(@PathVariable String country,
			@PathVariable String city) {
		return this.weatherService.getWeatherInfo(country, city);
	}

	@RequestMapping(method = RequestMethod.GET)
	public HttpEntity<?> getWeatherDetails() {
		return new ResponseEntity(weatherService.getDefaultWeatherDetails(), HttpStatus.OK);
	}
	
}

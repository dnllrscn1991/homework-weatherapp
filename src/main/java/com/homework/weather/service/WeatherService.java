package com.homework.weather.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriTemplate;

import com.homework.weather.HomeworkProperties;
import com.homework.weather.domain.WeatherInfo;

@Service
public class WeatherService {

	static final String WEATHER_URL = "http://api.openweathermap.org/data/2.5/weather?q={city},{country}&APPID={key}";

	private final RestTemplate restTemplate;

	private final String weatherMapKey;
	
	private final List<String> locations;

	public WeatherService(RestTemplateBuilder restTemplateBuilder,
			HomeworkProperties properties) {
		this.restTemplate = restTemplateBuilder.build();
		this.weatherMapKey = properties.getWeatherMap().getKey();
		this.locations = properties.getLocations();
	}

	public Object getDefaultWeatherDetails(){
		List<WeatherInfo> weatherDetails = new ArrayList<>();
		for(String location : this.locations) {
			String country = location.split("/")[0];
			String city = location.split("/")[1];
			weatherDetails.add(getWeatherInfo(country, city));
		}
		return weatherDetails;
	}
	
	
	public WeatherInfo getWeatherInfo(String country, String city) {
		URI url = new UriTemplate(WEATHER_URL).expand(city, country, this.weatherMapKey);
		return invoke(url, WeatherInfo.class);
	}
	


	private <T> T invoke(URI url, Class<T> responseType) {
		RequestEntity<?> request = RequestEntity.get(url).accept(MediaType.APPLICATION_JSON).build();
		ResponseEntity<T> exchange = this.restTemplate.exchange(request, responseType);
		return exchange.getBody();
	}
}

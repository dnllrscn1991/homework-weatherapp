package com.homework.weather.domain;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;


public class WeatherInfoRepository {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	public void insertWeatherLog(WeatherInfo weather) {		
		String script = String.format(
				"INSERT INTO WEATHER_LOG (RESPONSE_ID, LOCATION, ACTUAL_WEATHER, TEMPERATURE, DTIMEINSERTED) "
				+ "VALUES(%s, %s, %s, %s, %s)",
				weather.getWeatherId(), weather.getName(), weather.getActualWeather(), weather.getTemperature(), weather.getTimestamp());
		jdbcTemplate.execute(script);
	}
	
	public List<WeatherInfo> findAll(){
		String script = "SELECT TOP 5 * FROM WEATHER_LOG ORDER BY DTIMEINSERTED";
		return jdbcTemplate.query(script, new ResultSetExtractor<List<WeatherInfo>>() {
			public List<WeatherInfo> extractData(ResultSet rs) throws SQLException, DataAccessException{
				List<WeatherInfo> weatherReport = new ArrayList<>();
				while(rs.next()) {
					WeatherInfo report = new WeatherInfo();
					report.setWeatherId(Integer.parseInt(rs.getString("RESPONSE_ID")));
					report.setName(rs.getString("LOCATION"));
					report.setActualWeather(rs.getString("ACTUAL_WEATHER"));
					report.setTemperature(Double.parseDouble(rs.getString("TEMPERATURE")));
					report.setTimestamp(Long.parseLong(rs.getString("DTIMEINSERTED")));
					weatherReport.add(report);
				}
				return weatherReport;
			}
		});
	}

}
